import { Component } from '@angular/core';
import { Calculadora } from '../models/Calculadora';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  calculadora: Calculadora;
  resp: number;

  constructor() {
    this.calculadora = new Calculadora();
  }

  private calcular(operaçao: string){
    this.calculadora.operaçao = operaçao;
    this.resp = this.calculadora.calcular();
  }
}
